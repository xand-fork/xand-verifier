use crate::services::verifier_errors::Result;
use wasm_hash_verifier::{WasmBlob, XandHash};

/// Inputs and outputs to the core logic
pub trait XandHashVerifierService: XandCompilerService {
    fn make_hash(&self, _blob: WasmBlob<Vec<u8>>) -> XandHash;

    fn verify_blob_against_hash(&self, _blob: WasmBlob<Vec<u8>>, _proposal_hash: XandHash) -> bool;

    /// Default implementation
    fn verify_source_against_hash_and_blob(
        &self,
        source: &Self::Source,
        feature_flags: Vec<String>,
        proposal_hash: XandHash,
    ) -> Result<bool> {
        let my_blob = self.compile_source(source, feature_flags)?;
        let my_blob_matches_proposal_hash =
            self.verify_blob_against_hash(my_blob.clone(), proposal_hash.clone());
        let my_hash_matches_proposal_hash = self.make_hash(my_blob) == proposal_hash;
        Ok(my_blob_matches_proposal_hash && my_hash_matches_proposal_hash)
    }
}

pub trait XandCompilerService {
    type Source;

    fn compile_source(
        &self,
        _source: &Self::Source,
        _feature_flags: Vec<String>,
    ) -> Result<WasmBlob<Vec<u8>>>;
}
