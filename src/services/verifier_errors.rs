use std::result::Result as StdResult;
use thiserror::Error;

pub type Result<T> = StdResult<T, VerifierError>;

#[derive(Debug, Error)]
pub enum VerifierError {} // TODO implement in ADO 7572"
