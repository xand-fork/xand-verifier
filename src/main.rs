use clap::Parser;
use xand_verifier::cli::adapter::XandVerifierCli;
use xand_verifier::cli::commands::CliOpts;
use xand_verifier::verifier::XandVerifier;

fn main() {
    let args = CliOpts::parse();
    let verifier_service = XandVerifier::new();
    let cli = XandVerifierCli::new(verifier_service);
    cli.parse(args).expect("Command line parser panicked");
}
