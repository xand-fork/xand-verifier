#![allow(unused_variables)]

use crate::cli::commands::{CliOpts, Commands};
use crate::services::verifier_errors::Result;
use crate::services::xand_verifier_service::XandHashVerifierService;

pub struct XandVerifierCli<T: XandHashVerifierService>(T);

impl<T: XandHashVerifierService> XandVerifierCli<T> {
    pub fn new(caller: T) -> Self {
        Self(caller)
    }

    pub fn parse(&self, opts: CliOpts) -> Result<()> {
        match opts.command {
            Commands::CompileSource {
                source_path,
                feature_flags,
            } => {
                // TODO "Implement in ADO 7572"
            }
            Commands::MakeHash { wasm_path } => {
                // TODO "Implement in ADO 7572"
            }
            Commands::VerifyBlobAgainstHash { wasm_path, hash } => {
                // TODO "Implement in ADO 7572"
            }
            Commands::VerifySourceAgainstHashAndBlob {
                source_path,
                feature_flags,
                wasm_path,
                hash,
            } => {
                // TODO "Implement in ADO 7572"
            }
        }
        Ok(()) // TODO unreachable expression error
    }
}
