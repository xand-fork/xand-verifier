use clap::{Parser, Subcommand};
use std::path::PathBuf;

/// Xand Verifier CLI
#[derive(Debug, Parser)]
#[clap(name = "xand_verifier_cli")]
#[clap(
    about = "Xand Verifier CLI",
    long_about = "Tool to verify hashes from source code or wasm blobs"
)]
pub struct CliOpts {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Compile WASM blob from source, with optional flags
    #[clap(arg_required_else_help = true)]
    CompileSource {
        #[clap(long, required = true, parse(from_os_str))]
        source_path: PathBuf,
        #[clap(long, required = false)]
        feature_flags: Option<Vec<String>>,
    },
    /// Make hash
    #[clap(arg_required_else_help = true)]
    MakeHash {
        #[clap(long, required = true)]
        wasm_path: PathBuf,
    },
    /// Verify hash
    #[clap(arg_required_else_help = true)]
    VerifyBlobAgainstHash {
        #[clap(long, required = true, parse(from_os_str))]
        wasm_path: PathBuf,
        #[clap(long, required = true)]
        hash: String,
    },
    /// Verify source against hash
    #[clap(arg_required_else_help = true)]
    VerifySourceAgainstHashAndBlob {
        #[clap(long, required = true, parse(from_os_str))]
        source_path: PathBuf,
        #[clap(long, required = false)]
        feature_flags: Option<Vec<String>>,
        #[clap(long, required = true)]
        wasm_path: PathBuf,
        #[clap(long, required = true)]
        hash: String,
    },
}
