use crate::services::verifier_errors::Result;
use crate::services::xand_verifier_service::{XandCompilerService, XandHashVerifierService};
use wasm_hash_verifier::{WasmBlob, XandHash};

#[derive(Default)]
pub struct XandVerifier;

impl XandVerifier {
    pub fn new() -> Self {
        Self
    }
}

impl XandCompilerService for XandVerifier {
    type Source = ();

    fn compile_source(
        &self,
        _source: &Self::Source,
        _feature_flags: Vec<String>,
    ) -> Result<WasmBlob<Vec<u8>>> {
        todo!("Implement in ADO 7572 - use wasm-hash-verifier")
    }
}

/// Core App Verification Logic
impl XandHashVerifierService for XandVerifier {
    fn make_hash(&self, _blob: WasmBlob<Vec<u8>>) -> XandHash {
        todo!("Implement in ADO 7572")
    }

    fn verify_blob_against_hash(&self, _blob: WasmBlob<Vec<u8>>, _proposal_hash: XandHash) -> bool {
        todo!("Implement in ADO 7572 - use wasm-hash-verifier")
    }
}
