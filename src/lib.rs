/// CLI Interface provided as a wrapper over the lib
pub mod cli;
/// Port for accessing core application logic
pub mod services;
/// Core app logic
pub mod verifier;
