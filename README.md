# xand-verifier

Xand's standardized WasmBlob verification CLI and library

# Building

Run `cargo make build`

# Running

From the binary path, run 

`xand-verifier <SUBCOMMAND>`

For example:

`./target/release/xand-verifier <SUBCOMMAND>`

# CI / CD Artifacts

## Tag-To-Publish Stable

Push a tag in the form of v${VERSION} to release a stable version of the crate.

## Beta Artifacts

All Development branches have manual jobs for publishing a beta package.
