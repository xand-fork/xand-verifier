# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->

<!-- When a new release is made, add it here
e.g. ## [0.1.2] - 2020-11-09 -->

## [0.1.0] - 2022-06-13
### Added
- CI/CD Repo Setup 
- Library interface 
- CLI interface
- CLI client ports and adapters

